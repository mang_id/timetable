#ifndef TIMETABLE_H
#define TIMETABLE_H

#include <QMainWindow>
#include <QWidget>
#include <QTableView>
#include <QStandardItemModel>
#include <QListView>
#include <QMainWindow>
#include <QApplication>
#include <QStandardItem>
#include <QGridLayout>
#include <QVector>
#include <QFile>
#include <QString>
#include <QTextStream>
#include <QFileDialog>
#include <QVBoxLayout>
#include <QPushButton>
#include <QTableWidget>
#include <QMessageBox>


namespace Ui {
class TimeTable;
}

struct DATA
{
    QString subject;
    QString hours;
    QString day;
    QString room;
};

class TimeTable : public QMainWindow
{
    Q_OBJECT

public:
    explicit TimeTable(QWidget *parent = 0);
    QStandardItemModel *modelItems;
    void loadData(QString& adress, int i);
    void on_keywordsList_clicked(const QModelIndex &index);
    int columnSubjects = 0;
    int rowSubjects = 0;
    int columnHours = 0;
    int rowHours = 0;
    int columnRooms = 0;
    int rowSubRooms = 0;
    int columnDays = 0;
    int rowSubDays = 0;

    ~TimeTable();

private:
    Ui::TimeTable *ui;
    QTableWidget *tableWidget;
    QListView * listView;
    QPushButton * pbAdd;
    QVector<DATA*> data;
    QStringList weekdays;
    QStringList hours;
    int rows=0;
private slots:
    void on_pbAdd_clicked();
    void on_pbCheck_clicked();
};



#endif // TIMETABLE_H
