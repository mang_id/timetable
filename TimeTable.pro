#-------------------------------------------------
#
# Project created by QtCreator 2016-02-03T06:13:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TimeTable
TEMPLATE = app


SOURCES += main.cpp\
        timetable.cpp

HEADERS  += timetable.h

FORMS    += timetable.ui
