#include "timetable.h"
#include "ui_timetable.h"
#include <QTableWidgetItem>

TimeTable::TimeTable(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TimeTable)
{
    ui->setupUi(this);
    ui->tableWidget->setRowCount(++rows);
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() <<"Subject"<<"Clock"<<"Day"<<"Room");
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);


    QString array[] = {
        "Resources/Lists/ListOfSubjects.list",
        "Resources/Lists/ListOfHours.list",
        "Resources/Lists/ListOfAuditoriums.list"
        ,"Resources/Lists/ListOfWeekdays.list"};

    for(int i=0; i < 4; i++)
    {
        loadData(array[i], i);
    }

}

void TimeTable::loadData(QString& adress, int i)
{
    QStandardItemModel *modelItems  = new QStandardItemModel();
    QString cd=qApp->applicationDirPath();

    QFile file("/Users/iMac/Projects/QT/timetable/"+adress);
    QMessageBox::information(this,"File",cd);

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QMessageBox::information(this,"File","File not found");
    }
    QTextStream in(&file);
    QStandardItem * item;
    while(!in.atEnd())
    {
         QString line = in.readLine();
         if(i==1) // hours
             hours.append(line);
         else if(i==3) // days
             weekdays.append(line);
         item=new QStandardItem(line);
         modelItems->appendRow(item);
    }
    switch (i)
    {
    case 0:    ui->lvSubject->setModel(modelItems);
        break;
    case 1:    ui->lvHours->setModel(modelItems);
        break;
    case 2:    ui->lvRoom->setModel(modelItems);
        break;
    case 3:    ui->lvDays->setModel(modelItems);
        break;
    }
    file.close();

}


void TimeTable::on_pbAdd_clicked()
{
    QModelIndexList listSubjects=ui->lvSubject->selectionModel()->selectedIndexes();
    QModelIndexList listHours=ui->lvHours->selectionModel()->selectedIndexes();
    QModelIndexList listRoom=ui->lvRoom->selectionModel()->selectedIndexes();
    QModelIndexList listDays=ui->lvDays->selectionModel()->selectedIndexes();

    DATA *item=new DATA;

    const QModelIndex& index1=listSubjects[0];
    QString lineSubjects = index1.data(Qt::DisplayRole).toString();
    const QModelIndex& index2=listHours[0];
    QString lineHours = index2.data(Qt::DisplayRole).toString();
    const QModelIndex& index3=listDays[0];
    QString lineDays = index3.data(Qt::DisplayRole).toString();
    const QModelIndex& index4=listRoom[0];
    QString lineRoom = index4.data(Qt::DisplayRole).toString();

    for(int i=0;i<data.count();i++)
    {
        //QMessageBox::information(this,"",data.at(i)->day);
        if(data.at(i)->day==lineDays && data.at(i)->hours==lineHours)
        {
            QMessageBox::critical(this,"Error","Days and Hours are the same!");
            return;
        }
    }

    item->subject=lineSubjects;
    item->hours=lineHours;
    item->day=lineDays;
    item->room=lineRoom;

    ui->tableWidget->setItem(columnSubjects, 0, new QTableWidgetItem(lineSubjects));
    QTableWidgetItem *temp = ui->tableWidget->item(columnSubjects, 0);
    QString str = temp->text();
    columnSubjects++;
    ui->tableWidget->setItem(columnHours, 1, new QTableWidgetItem(lineHours));
    temp = ui->tableWidget->item(columnHours, 1);
    str = temp->text();
    columnHours++;
    ui->tableWidget->setItem(columnDays, 2, new QTableWidgetItem(lineDays));
    temp = ui->tableWidget->item(columnDays, 2);
    str = temp->text();
    columnDays++;
    ui->tableWidget->setItem(columnRooms, 3, new QTableWidgetItem(lineRoom));
    temp = ui->tableWidget->item(columnRooms, 3);
    str = temp->text();
    columnRooms++;

    data.push_back(item);
    ui->tableWidget->setRowCount(++rows);
 }

TimeTable::~TimeTable()
{
    delete ui;
}

void TimeTable::on_pbCheck_clicked()
{
    QMap<QString,int> week;
    QString freeDays="";
    int free=0;
    foreach(const QString& day,weekdays)
        week[day]=0;

    for(int i=0;i<data.count();i++)
        week[data.at(i)->day]++;

    for(auto day : week.keys())
        if(week.value(day)==0) {
            freeDays+=day+" ";
            free++;
        }

    if(free)
       QMessageBox::information(this,"","Free days: "+freeDays);
    else
       QMessageBox::information(this,"","No free days! "+freeDays);
}
